Test case name
	free shipping eligibility
Pre-request(test-setup, test-teardown)
	open browser
	maximize browser window
	close all browsers
Test step 1
	Go to main Page
		navigate to main page
		verify page loaded
    	add product to chart
Test step 2
	Go to checkout page
  		navigate to checkout page
    	get total amount
Test step 3
	Validate Calculation
	    compare total amount with 50 TL
		calculate amount needed until reach free shipping amount