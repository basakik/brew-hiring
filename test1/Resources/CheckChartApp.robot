*** Settings ***
Library  SeleniumLibrary
Resource  ./PO/main.robot
Resource  ./PO/checkout.robot
Resource  ./PO/calculate.robot
Documentation  This test suite, adds products from "Günün Fırsatları" and calculates the amount left for free shipping

*** Variables ***


*** Keywords ***
Go to Main Page
#This page includes "Günün Fırsatları"

    navigate to main page
    verify page loaded
    add product to chart


Go to Checkout Page
#This page includes "total amount"
    navigate to checkout page
    get total amount


Validate Calculation
#This calculation checks amount left for free shipping
    compare total amount with 50 TL
    calculate amount needed until reach free shipping amount

