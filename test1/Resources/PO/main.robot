*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${BROWSER} =  chrome
${URL} =  https://www.hepsiburada.com/
${CHECKOUT_URL} =  https://www.hepsiburada.com/ayagina-gelsin/sepetim
${PRODUCT_URL1} =  //*[@id="dealoftheday_5"]/div/div/div[2]/div/div/div/div/div[2]/div/div/a/div[4]/button/span
${DEALS} =  //*[@id="dealoftheday_5"]/div/div/div[2]/div/div/div/div/div[2]/div

*** Keywords ***
Navigate to Main Page
    go to  ${URL}
    sleep  4s
Add Product to Chart
    mouse over  ${DEALS}
    sleep  3s
    click element   ${PRODUCT_URL1}
    sleep  3s
Verify Page Loaded

    wait until page contains element  ${DEALS}
    sleep  3s
p  2s